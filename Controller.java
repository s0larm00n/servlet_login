package com.mkyong;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        PrintWriter out = response.getWriter();

        out.println("<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\"");
        out.println("pageEncoding=\"utf-8\"%>");
        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
        out.println("<title>Log in page</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<form action=\"cont\" method=\"get\">");
        out.println("<input type=\"hidden\" name=\"command\" value=\"forward\" />");
        out.println(" Enter login:<br/>");
        out.println("<input type=\"text\" name=\"login\" value=\"\" /><br/>");
        out.println(" Enter password:<br/>");
        out.println("<input type=\"password\" name=\"password\" value=\"\" /><br/>");
        out.println("<input type=\"submit\" value=\"Log in\" /><br/>");
        out.println("</form>\n</body></html> ");

        out.println("Your login: " + login);
        out.println("<br />Your password: " + password);

        //RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
        //requestDispatcher.forward(request, response);



    }


}